
from tensorflow.keras import backend as K
from tensorflow.keras import layers, Model, Input, models
try:
    from efficient_capsnet_layers import PrimaryCaps, FCCaps, Length, Mask
except ImportError:
    from .efficient_capsnet_layers import PrimaryCaps, FCCaps, Length, Mask
import numpy as np

K.set_image_data_format('channels_last')


def EfficientCapsNet(input_shape, n_class):
    print("starting EfficientCapsNet", input_shape, n_class)
    inputs_x = layers.Input(input_shape)
    y_true = layers.Input(shape=(n_class,))
    noise = layers.Input(shape=(n_class, 16))

    # efficient_capsnet = efficient_capsnet_graph(input_shape, n_class)

    print(inputs_x)
    x = layers.Conv2D(32, 5, activation="relu", padding='valid',
                      kernel_initializer='he_normal')(inputs_x)
    # print(x)
    x = layers.BatchNormalization()(x)
    # print(x)
    x = layers.Conv2D(64, 3, activation='relu', padding='valid',
                      kernel_initializer='he_normal')(x)
    # print(x)
    x = layers.BatchNormalization()(x)
    # print(x)
    x = layers.Conv2D(64, 3, activation='relu', padding='valid',
                      kernel_initializer='he_normal')(x)
    # print(x)
    x = layers.BatchNormalization()(x)
    print(x)
    x = layers.Conv2D(64, 3, activation='relu',
                      padding='valid', kernel_initializer='he_normal')(x)
    # print(x)
    x = layers.BatchNormalization()(x)
    # print(x)
    x = layers.Conv2D(64, 3, activation='relu',
                      padding='valid', kernel_initializer='he_normal')(x)
    # print(x)
    x = layers.BatchNormalization()(x)
    # print(x)
    x = layers.Conv2D(128, 3, 2, activation='relu',
                      padding='valid', kernel_initializer='he_normal')(x)
    # print(x)
    x = layers.BatchNormalization()(x)
    n_dims = 8
    n_caps = 16
    n_filters = n_dims * n_caps
    print(x)
    x = PrimaryCaps(n_filters, 9, n_caps, n_dims)(x)

    # print(x)

    digit_caps = FCCaps(10, 16)(x)

    digit_caps_len = Length(name='length_capsnet_output')(digit_caps)

    decoder = models.Sequential(name='decoder')
    decoder.add(layers.Dense(512, activation='relu', input_dim=16 * n_class))
    decoder.add(layers.Dense(1024, activation='relu'))
    decoder.add(layers.Dense(np.prod(input_shape), activation='sigmoid'))
    decoder.add(layers.Reshape(target_shape=input_shape, name='out_recon'))

    noised_digitcaps = layers.Add()(
        [digit_caps, noise])  # only if mode is play

    masked_by_y = Mask()([digit_caps, y_true])
    masked = Mask()(digit_caps)
    masked_noised_y = Mask()([noised_digitcaps, y_true])

    x_gen_train = decoder(masked_by_y)
    x_gen_eval = decoder(masked)
    x_gen_play = decoder(masked_noised_y)

    train_model = Model([inputs_x, y_true], [digit_caps_len,
                        x_gen_train], name='Efficinet_CapsNet_Generator')
    eval_model = Model(
        inputs_x, [digit_caps_len, x_gen_eval], name='Efficinet_CapsNet_Generator')
    manipulate_model = Model([inputs_x, y_true, noise], [
                             digit_caps_len, x_gen_play], name='Efficinet_CapsNet_Generator')

    return train_model, eval_model, manipulate_model
