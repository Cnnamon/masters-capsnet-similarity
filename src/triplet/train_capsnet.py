from data import visualize_image, visualize_images, get_train_valid_datasets, get_dataset_with_transformations
from read_args import readArgs
from capsnet import CapsNet, load_mnist
import tensorflow as tf
from tensorflow.keras import backend as K
from tensorflow.keras import layers, models, optimizers
from tensorflow.keras import callbacks
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import numpy as np

K.set_image_data_format('channels_last')


def margin_loss(y_true, y_pred):
    print("margin_loss", y_true, y_pred)
    tf.print(y_true)
    tf.print(y_pred)
    """
    Margin loss for Eq.(4). When y_true[i, :] contains not just one `1`, this loss should work too. Not test it.
    :param y_true: [None, n_classes]
    :param y_pred: [None, num_capsule]
    :return: a scalar loss value.
    """
    return tf.reduce_mean(tf.square(y_pred))
    # L = y_true * tf.square(tf.maximum(0., 0.9 - y_pred)) + \
    #     0.5 * (1 - y_true) * tf.square(tf.maximum(0., y_pred - 0.1))
    # # print('L', L)
    # tf.print(L)
    # mean = tf.reduce_mean(tf.reduce_sum(L, 1))
    # tf.print(mean)
    # return mean


def train(model,  # type: models.Model
          data, args):
    """
    Training a CapsuleNet
    :param model: the CapsuleNet model
    :param data: a tuple containing training and testing data, like `((x_train, y_train), (x_test, y_test))`
    :param args: arguments
    :return: The trained model
    """
    # unpacking the data
    (x_train, y_train), (x_test, y_test) = data

    # callbacks
    log = callbacks.CSVLogger(args.save_dir + '/log.csv')
    checkpoint = callbacks.ModelCheckpoint(args.save_dir + '/weights-{epoch:02d}.h5', monitor='val_capsnet_accuracy',
                                           save_best_only=True, save_weights_only=True, verbose=1)
    lr_decay = callbacks.LearningRateScheduler(
        schedule=lambda epoch: args.lr * (args.lr_decay ** epoch))
    tensorboard_callback = callbacks.TensorBoard(
        log_dir='./result/logs', histogram_freq=1)

    # compile the model
    model.compile(optimizer=optimizers.Adam(lr=args.lr),
                  loss=[margin_loss, 'mse'],
                  loss_weights=[1., args.lam_recon],
                  metrics={'capsnet': 'accuracy'})
    tf.keras.utils.plot_model(model, to_file="model.png", show_shapes=True)

    """
    # Training without data augmentation:
    model.fit([x_train, y_train], [y_train, x_train], batch_size=args.batch_size, epochs=args.epochs,
              validation_data=[[x_test, y_test], [y_test, x_test]], callbacks=[log, tb, checkpoint, lr_decay])
    """

    # Begin: Training with data augmentation ---------------------------------------------------------------------#
    def train_generator(x, y, batch_size, shift_fraction=0.):
        train_datagen = ImageDataGenerator(width_shift_range=shift_fraction,
                                           height_shift_range=shift_fraction)  # shift up to 2 pixel for MNIST
        generator = train_datagen.flow(x, y, batch_size=batch_size)
        for next_generated in generator:
            yield (next_generated[0], next_generated[1]), (next_generated[1], next_generated[0])

    def train_generator_test(x, batch_size, shift_fraction=0.):
        train_datagen = ImageDataGenerator(width_shift_range=shift_fraction,
                                           height_shift_range=shift_fraction)  # shift up to 2 pixel for MNIST
        generator = train_datagen.flow(x.take(1000), batch_size=batch_size)
        for next_generated in generator:
            yield (next_generated[0], next_generated[1]), (next_generated[1], next_generated[0])

    # x_train = list(x_train.take(1).as_numpy_iterator())
    # y_train = list(y_train.take(1).as_numpy_iterator())
    train_ds = tf.data.Dataset.zip(
        ((x_train, y_train), (y_train, x_train)))
    for element in y_train.as_numpy_iterator():
        print(element)
    # Training with data augmentation. If shift_fraction=0., no augmentation.
    model.fit(  # train_data,
        train_ds,
        #steps_per_epoch=int(y_train.shape[0] / args.batch_size),
        epochs=args.epochs,
        validation_data=(x_test, y_test),
        batch_size=args.batch_size)
    # callbacks=[log, checkpoint, lr_decay, tensorboard_callback])
    # End: Training with data augmentation -----------------------------------------------------------------------#

    model.save_weights(args.save_dir + '/trained_model.h5')
    print('Trained model saved to \'%s/trained_model.h5\'' % args.save_dir)

    from utils import plot_log
    plot_log(args.save_dir + '/log.csv', show=True)

    return model


def load_model(args):
    ''' load data '''
    x_dataset, y_dataset, image_count = get_dataset_with_transformations(
        args.train_dir)
    return
    x_train, y_train, x_valid, y_valid = get_train_valid_datasets(
        x_dataset, y_dataset, image_count, args.batch_size)
    # print("dataset", train_dataset, type(train_dataset))
    # for images, labels in train_dataset.take(1):
    #     print('images.shape: ', images.shape)
    #     print('labels.shape: ', labels.shape)

    ''' test sample '''
    sample = next(iter(x_train))
    # class_shape = sample[0][1].shape
    # print("class_shape", class_shape)
    # print("sample", sample[0].shape, sample[1].shape)
    # print("iterator", train_dataset.as_numpy_iterator())

    ''' print image '''
    # images = []
    # for i in range(10):
    #     images.append(list(train_dataset.take(1).as_numpy_iterator())[0])
    # print(type(images[0]), images[0][0].shape)
    # visualize_images(images)
    # (x_train, y_train), (x_test, y_test) = load_mnist()

    ''' define model '''
    model, eval_model, manipulate_model = CapsNet(input_shape=sample.shape[1:],
                                                  n_class=10,
                                                  routings=args.routings,
                                                  batch_size=args.batch_size)
    model.summary()
    # train or test
    if args.weights is not None:  # init the model weights with provided one
        model.load_weights(args.weights)
    if not args.testing:
        train(model=model, data=((x_train, y_train), (x_valid, y_valid)),
              args=args)
    else:  # as long as weights are given, will run testing
        if args.weights is None:
            print('No weights are provided. Will test using random initialized weights.')
        # manipulate_latent(manipulate_model, validate_dataset, args)
        # test(model=eval_model, data=validate_dataset, args=args)


args = readArgs()
load_model(args)
