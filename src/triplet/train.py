import os
from model import create_siamese_model, create_siamese_capsnet_model, inspect_siamese_model, create_siamese_embedding_model, create_siamese_caps_net_embedding_model, input_shape
from data import visualize, get_train_valid_datasets
from capsnet import margin_loss
from tensorflow.keras import optimizers, callbacks


def train(args):

    # data
    # input_shape_build = (None,) + input_shape
    train_dataset, validate_dataset = get_train_valid_datasets(
        args.train_dir, args.batch_size)

    # models
    # embeddings = create_siamese_embedding_model()
    # siamese_model = create_siamese_model(embeddings)
    embeddings = create_siamese_caps_net_embedding_model(
        args.routings, args.batch_size)
    siamese_model = create_siamese_capsnet_model(embeddings)

    # callbacks
    # log = callbacks.CSVLogger(args.save_dir + '/log.csv')
    # checkpoint = callbacks.ModelCheckpoint(args.save_dir + '/weights-{epoch:02d}.h5', monitor='val_accuracy',
    #                                        save_best_only=True, save_weights_only=True, verbose=1)
    # lr_decay = callbacks.LearningRateScheduler(
    #     schedule=lambda epoch: args.lr * (args.lr_decay ** epoch))

    # train
    siamese_model.compile(optimizer=optimizers.Adam(0.0001))  # lr=args.lr
    # , loss=[margin_loss, 'mse'], loss_weights=[1., args.lam_recon],
    siamese_model.fit(train_dataset, epochs=args.epochs,
                      validation_data=validate_dataset, batch_size=args.batch_size)  # callbacks=[log, checkpoint, lr_decay]
    # siamese_model.build(
    #     [input_shape_build, input_shape_build, input_shape_build])
    # siamese_model.summary()
    embeddings.summary()

    for layer in embeddings.layers:
        if 'conv' not in layer.name:
            continue
        filters, biases = layer.get_weights()
        print(layer.name, filters.shape)

        # normalize filter values to 0-1 so we can visualize them
        f_min, f_max = filters.min(), filters.max()
        filters = (filters - f_min) / (f_max - f_min)

    # siamese_model.get_weights()
    # test sample
    sample = next(iter(train_dataset))
    # print(sample)
    inspect_siamese_model(sample, embeddings)
    # sample = next(iter(train_dataset))
    # visualize(*list(train_dataset.take(1).as_numpy_iterator())[0])
    # visualize(*sample)

    # train_gen, valid_gen, num_steps_train, num_steps_valid = load_data(
    #     args.train_dir, args.valid_dir, args.batch_size)
    # print(next(train_gen))
    # model = create_model()
    # model.summary()
    # model.compile(loss="categorical_crossentropy",
    #               optimizer="adam", metrics=["accuracy"])
    # history = model.fit(train_gen,
    #                     steps_per_epoch=num_steps_train,
    #                     epochs=args.epochs,
    #                     validation_data=valid_gen,
    #                     validation_steps=num_steps_valid)
