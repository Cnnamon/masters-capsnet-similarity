import tensorflow as tf
from tensorflow.keras import Model, layers, metrics, models
from capsnet_modif import cosine_distance, cosine_distance_output_shape, create_base_network, CapsNet, margin_loss
from data import RESIZE_HEIGHT, RESIZE_WIDTH, visualize
from tensorflow.keras.applications import resnet
from siamese_model import SiameseModel
from siamese_capsnet_model import CapsNetSiameseModel
import numpy as np

input_shape = (RESIZE_WIDTH, RESIZE_HEIGHT) + (3,)


def create_model():
    base_network = create_base_network(input_shape)

    image_left = layers.Input(shape=input_shape)
    image_right = layers.Input(shape=input_shape)

    vector_left = base_network(image_left)
    vector_right = base_network(image_right)

    # distance = Lambda(cosine_distance,
    #                   output_shape=cosine_distance_output_shape)([out_caps_left, out_caps_right])

    distance = layers.Lambda(cosine_distance,
                             output_shape=cosine_distance_output_shape)([vector_left, vector_right])

    fc1 = layers.Dense(128, kernel_initializer="glorot_uniform")(distance)
    fc1 = layers.Dropout(0.2)(fc1)
    fc1 = layers.Activation("relu")(fc1)
    pred = layers.Dense(2, kernel_initializer="glorot_uniform")(fc1)
    pred = layers.Activation("softmax")(pred)
    # model = Model(inputs=[x_left, x_right], outputs=pred)
    model = Model(inputs=[image_left, image_right], outputs=pred)
    return model


def create_siamese_embedding_model():
    base_cnn = resnet.ResNet50(
        weights="imagenet", input_shape=input_shape, include_top=False
    )
    flatten = layers.Flatten()(base_cnn.output)
    dense1 = layers.Dense(512, activation="relu")(flatten)
    dense1 = layers.BatchNormalization()(dense1)
    dense2 = layers.Dense(256, activation="relu")(dense1)
    dense2 = layers.BatchNormalization()(dense2)
    output = layers.Dense(256)(dense2)
    embedding = Model(base_cnn.input, output, name="Embedding")
    trainable = False
    for layer in base_cnn.layers:
        if layer.name == "conv5_block1_out":
            trainable = True
        layer.trainable = trainable
    return embedding


def create_siamese_caps_net_embedding_model(routings, batch_size, n_class = 2):
    x, out = CapsNet(input_shape=input_shape, n_class=n_class,
                     routings=routings, batch_size=batch_size)
    flatten = layers.Flatten()(out)
    dense1 = layers.Dense(512, activation="relu",
                          input_dim=16 * n_class)(flatten)
    dense1 = layers.BatchNormalization()(dense1)
    dense2 = layers.Dense(256, activation="relu")(dense1)
    dense2 = layers.BatchNormalization()(dense2)
    output = layers.Dense(256)(dense2)
    # decoder = models.Sequential()
    # decoder.add(layers.Dense(512, activation='relu', input_dim=16 * n_class))
    # decoder.add(layers.Dense(1024, activation='relu'))
    # decoder.add(layers.Dense(np.prod(input_shape), activation='sigmoid'))
    # decoder.add(layers.Reshape(target_shape=input_shape))
    # output = decoder(out)
    embedding = Model(x, output, name="Embedding")
    return embedding


class DistanceLayer(layers.Layer):
    """
    This layer is responsible for computing the distance between the anchor
    embedding and the positive embedding, and the anchor embedding and the
    negative embedding.
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def call(self, anchor, positive, negative):
        ap_distance = tf.reduce_sum(tf.square(anchor - positive), -1)
        an_distance = tf.reduce_sum(tf.square(anchor - negative), -1)
        return (ap_distance, an_distance)

def create_siamese_network(embedding):
    anchor_input = layers.Input(name="anchor", shape=input_shape)
    positive_input = layers.Input(name="positive", shape=input_shape)
    negative_input = layers.Input(name="negative", shape=input_shape)
    distances = DistanceLayer()(
        embedding(resnet.preprocess_input(anchor_input)),
        embedding(resnet.preprocess_input(positive_input)),
        embedding(resnet.preprocess_input(negative_input)),
    )
    siamese_network = Model(
        inputs=[anchor_input, positive_input,
                negative_input], outputs=distances
    )
    return siamese_network

def create_siamese_model(embedding):
    siamese_network = create_siamese_network(embedding)
    siamese_model = SiameseModel(siamese_network)
    return siamese_model

def create_siamese_capsnet_model(embedding):
    siamese_network = create_siamese_network(embedding)
    siamese_model = CapsNetSiameseModel(siamese_network)
    return siamese_model


def inspect_siamese_model(sample, embedding):
    anchor, positive, negative = sample
    anchor_embedding, positive_embedding, negative_embedding = (
        embedding(resnet.preprocess_input(anchor)),
        embedding(resnet.preprocess_input(positive)),
        embedding(resnet.preprocess_input(negative)),
    )

    cosine_similarity = metrics.CosineSimilarity()

    positive_similarity = cosine_similarity(
        anchor_embedding, positive_embedding)
    print("Positive similarity:", positive_similarity.numpy())

    negative_similarity = cosine_similarity(
        anchor_embedding, negative_embedding)
    print("Negative similarity:", negative_similarity.numpy())
    visualize(anchor, positive, negative)
