import numpy as np
from matplotlib import pyplot as plt
import csv
import math
import pandas


def plot_log(filename, show=True):

    data = pandas.read_csv(filename)

    fig = plt.figure(figsize=(4, 6))
    fig.subplots_adjust(top=0.95, bottom=0.05, right=0.95)
    fig.add_subplot(211)
    for key in data.keys():
        if key.find('loss') >= 0 and not key.find('val') >= 0:  # training loss
            plt.plot(data['epoch'].values, data[key].values, label=key)
    plt.legend()
    plt.title('Training loss')

    fig.add_subplot(212)
    for key in data.keys():
        if key.find('acc') >= 0:  # acc
            plt.plot(data['epoch'].values, data[key].values, label=key)
    plt.legend()
    plt.title('Training and validation accuracy')

    # fig.savefig('result/log.png')
    if show:
        plt.show()


def combine_images(generated_images, height=None, width=None):
    num = generated_images.shape[0]
    if width is None and height is None:
        width = int(math.sqrt(num))
        height = int(math.ceil(float(num)/width))
    elif width is not None and height is None:  # height not given
        height = int(math.ceil(float(num)/width))
    elif height is not None and width is None:  # width not given
        width = int(math.ceil(float(num)/height))

    shape = generated_images.shape[1:3]
    image = np.zeros((height*shape[0], width*shape[1]),
                     dtype=generated_images.dtype)
    for index, img in enumerate(generated_images):
        i = int(index/width)
        j = index % width
        image[i*shape[0]:(i+1)*shape[0], j*shape[1]:(j+1)*shape[1]] = \
            img[:, :, 0]
    return image


test_data_list = [(range(0, 100), 'Road and field'),
                  (range(100, 200), 'Lake and road'),
                  (range(200, 300), 'forest in field'),
                  (range(300, 400), '2 Lakes and road'),
                  (range(400, 500), 'Road and forest'),
                  (range(500, 600), 'Forest with shape'),
                  (range(600, 700), '90% Forest'),
                  (range(700, 800), 'All Forest'),
                  (range(800, 900), 'Urban'),
                  (range(900, 1000), 'Suburbs')]


def show_graph(_range, title, x_recon, emb_size, path):
    margin = 0.01
    tp = 0
    tp_m = 0  # with small margin
    fp_m = 0  # with small margin
    fp = 0
    xaxis = []
    yaxis = []
    for i in _range:
        # ds_i = int(i%100/10)
        embedding_img_a = x_recon[i, :emb_size]
        embedding_img_p = x_recon[i, emb_size:2*emb_size]
        embedding_img_n = x_recon[i, 2*emb_size:]
        # embedding_img_a.shape, embedding_img_p.shape, embedding_img_n.shape
        p = np.mean(np.square(embedding_img_a - embedding_img_p))
        n = np.mean(np.square(embedding_img_a - embedding_img_n))
        # print(p, n)
        pda = np.nansum(p)
        pda_m = pda + margin
        nda = np.nansum(n)
        # print(pda, pda_m, nda)
        xaxis.append(i % 100)
        yaxis.append([pda, nda])
        # print(pda, nda)
        if pda_m < nda:
            tp_m += 1
        else:
            fp_m += 1
        if pda < nda:
            tp += 1
        else:
            fp += 1
        # print(np.max(p), np.max(n), np.max(p - n), np.max(p - n + 0.2), p < n)
    acc = np.round(tp / (tp + fp) * 100, 1)
    acc_m = np.round(tp_m / (tp_m + fp_m) * 100, 1)
    plt.plot(xaxis, yaxis)
    plt.title(title + ', Accuracy ' + str(acc) + ' (' + str(acc_m) + ')')
    plt.savefig(path + title + '.png')
    plt.show()
    return acc, acc_m


def show_graphs(x_recon, emb_size, path):
    total_acc = 0
    total_acc_m = 0
    for data_iter in test_data_list:
        acc, acc_m = show_graph(
            data_iter[0], data_iter[1], x_recon, emb_size, path)
        total_acc += acc
        total_acc_m += acc_m
    print(total_acc/1000, total_acc_m/1000)


if __name__ == "__main__":
    plot_log('result/log.csv')
