import os
from pathlib import Path
import math
import matplotlib.pyplot as plt
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import tensorflow as tf
import tensorflow_addons as tfa
import numpy as np
from PIL import Image

RESIZE_WIDTH = 28  # 128  # 224
RESIZE_HEIGHT = 28  # 128  # 224
TARGET_SHAPE = (RESIZE_WIDTH, RESIZE_HEIGHT)
DATAGEN_ARGS = dict(rotation_range=1,
                    width_shift_range=0.01,
                    height_shift_range=0.01,
                    shear_range=0.1,
                    zoom_range=0.1,
                    horizontal_flip=True)
cached_images = {}


def draw_image(subplot, image, title):
    plt.subplot(subplot)
    plt.imshow(image)
    plt.title(title)
    plt.xticks([])
    plt.yticks([])


def image_triple_generator_demo(image_triples, batch_size):
    while True:
        # loop once per epoch
        num_recs = len(image_triples)
        indices = np.random.permutation(np.arange(num_recs))
        num_batches = num_recs // batch_size
        for bid in range(num_batches):
            # loop once per batch
            batch_indices = indices[bid * batch_size: (bid + 1) * batch_size]
            yield [image_triples[i] for i in batch_indices]


def read_image_to_cache(image_path):
    if image_path not in cached_images:
        image = Image.open(image_path)
        im_arr = np.array(image.resize((RESIZE_WIDTH, RESIZE_HEIGHT)))
        im_arr32 = im_arr.astype(np.float32) / 255
        cached_images[image_path] = im_arr32
    return cached_images[image_path]


def draw_resized_image(image_path):
    datagen = ImageDataGenerator(**DATAGEN_ARGS)
    image = Image.open(image_path)
    image = image.resize((RESIZE_WIDTH, RESIZE_HEIGHT))
    image = datagen.random_transform(np.array(image))
    image = Image.fromarray(image, 'RGB')
    image.show()


def preprocess_images(image_names, seed, datagen, data_dir):
    np.random.seed(seed)
    X = np.zeros((len(image_names), RESIZE_WIDTH, RESIZE_HEIGHT, 3))
    for i, image_name in enumerate(image_names):
        image = read_image_to_cache(os.path.join(data_dir, image_name))
        X[i] = datagen.random_transform(image)
    return X


def image_triple_generator(image_triples, batch_size, data_dir):
    datagen_args = dict(rotation_range=10,
                        width_shift_range=0.2,
                        height_shift_range=0.2,
                        shear_range=0.2,
                        zoom_range=0.2,
                        horizontal_flip=True)
    datagen_left = ImageDataGenerator(**datagen_args)
    datagen_right = ImageDataGenerator(**datagen_args)

    while True:
        # loop once per epoch
        num_recs = len(image_triples)
        indices = np.random.permutation(np.arange(num_recs))
        num_batches = num_recs // batch_size
        for bid in range(num_batches):
            # loop once per batch
            batch_indices = indices[bid * batch_size: (bid + 1) * batch_size]
            batch = [image_triples[i] for i in batch_indices]
            # make sure the two image data generators generate same transformations
            seed = np.random.randint(low=0, high=1000, size=1)[0]
            Xleft = preprocess_images(
                [b[0] for b in batch], seed, datagen_left, data_dir)
            Xright = preprocess_images(
                [b[1] for b in batch], seed, datagen_right, data_dir)
            Y = to_categorical(np.array([b[2] for b in batch]))
            yield ([Xleft, Xright], Y)


def generate_image_triples_batch(data_dir, image_triples, batch_size, shuffle=False):
    while True:
        # loop once per epoch
        if shuffle:
            indices = np.random.permutation(np.arange(len(image_triples)))
        else:
            indices = np.arange(len(image_triples))
        shuffled_triples = [image_triples[ix] for ix in indices]
        num_batches = len(shuffled_triples) // batch_size
        for bid in range(num_batches):
            # loop once per batch
            images_left, images_right, labels = [], [], []
            batch = shuffled_triples[bid * batch_size: (bid + 1) * batch_size]
            for i in range(batch_size):
                lhs, rhs, label = batch[i]
                images_left.append(read_image_to_cache(
                    os.path.join(data_dir, lhs)))
                images_right.append(read_image_to_cache(
                    os.path.join(data_dir, rhs)))
                labels.append(label)
            Xlhs = np.array(images_left)
            Xrhs = np.array(images_right)
            Y = to_categorical(np.array(labels), num_classes=2)
            yield ([Xlhs, Xrhs], Y)


def display_triplet(train_dir):
    ref_image = plt.imread(os.path.join(train_dir, "image-0000.png"))
    sim_image = plt.imread(os.path.join(train_dir, "image-0001.png"))
    dif_image = plt.imread(os.path.join(train_dir, "image-0002.png"))
    draw_image(131, ref_image, "reference")
    draw_image(132, sim_image, "similar")
    draw_image(133, dif_image, "different")
    plt.tight_layout()
    plt.show()


def display_transform(train_dir):
    datagen_args = dict(rotation_range=10,
                        width_shift_range=0.01,
                        height_shift_range=0.01,
                        shear_range=0.2,
                        zoom_range=0.2,
                        horizontal_flip=True)
    datagen = ImageDataGenerator(**datagen_args)
    sid = 150
    np.random.seed(42)
    image = plt.imread(os.path.join(train_dir, "image-0010.png"))
    sid += 1
    draw_image(sid, image, "orig")
    for j in range(4):
        augmented = datagen.random_transform(image)
        sid += 1
        draw_image(sid, augmented, "aug#{:d}".format(j + 1))
    plt.tight_layout()
    plt.show()


def get_image_triplet_list(data_dir):
    image_triplets = []  # (anchor, positive/negative, isSimilar)
    i = 1
    for image_dir in os.listdir(data_dir):
        if(i % 3 == 1):
            anchor_image = image_dir
        elif(i % 3 == 2):
            positive_image = image_dir
        elif(i % 3 == 0):
            negative_image = image_dir
            image_triplets.append((anchor_image, positive_image, 1))
            image_triplets.append((anchor_image, negative_image, 0))
        i += 1
    return image_triplets


def get_image_triplet_list_2(data_dir):
    anchor = []
    positive = []
    negative = []
    i = 1
    for image_dir in os.listdir(data_dir):
        if(i % 3 == 1):
            anchor.append(image_dir)
        elif(i % 3 == 2):
            positive.append(image_dir)
        elif(i % 3 == 0):
            negative.append(image_dir)
        i += 1
    return anchor, positive, negative


transform_minus5_suffix = 'transform+5'
transform_minus10_suffix = 'transform+10'
transform_plus5_suffix = 'transform-5'
transform_plus10_suffix = 'transform-10'


def get_image_triplet_list_for_transformation(data_dir):
    images = []
    labels = []
    for image_dir in os.listdir(data_dir):
        def add_transform(angle):
            images.append(image_dir)
            labels.append(angle)
        add_transform([1., 0., 0., 0., 0., 0., 0., 0., 0., 0.])
        add_transform([0., 1., 0., 0., 0., 0., 0., 0., 0., 0.])
        add_transform([0., 0., 1., 0., 0., 0., 0., 0., 0., 0.])
        add_transform([0., 0., 0., 1., 0., 0., 0., 0., 0., 0.])
        add_transform([0., 0., 0., 0., 1., 0., 0., 0., 0., 0.])
        add_transform([0., 0., 0., 0., 0., 1., 0., 0., 0., 0.])
        add_transform([0., 0., 0., 0., 0., 0., 1., 0., 0., 0.])
        add_transform([0., 0., 0., 0., 0., 0., 0., 1., 0., 0.])
        add_transform([0., 0., 0., 0., 0., 0., 0., 0., 1., 0.])
        add_transform([0., 0., 0., 0., 0., 0., 0., 0., 0., 1.])
    return (images, to_categorical(labels, num_classes=10))


def preprocess_image(filename):
    """
    Load the specified file as a JPEG image, preprocess it and
    resize it to the target shape.
    """

    image_string = tf.io.read_file(filename)
    image = tf.image.decode_png(image_string, channels=3)
    image = tf.image.convert_image_dtype(image, tf.float32)
    image = tf.image.resize(image, TARGET_SHAPE)
    return image


def preprocess_image_with_angle(filename, label):
    # print(filename, label)
    """
    Load the specified file as a JPEG image, preprocess it and
    resize it to the target shape.
    """

    image_string = tf.io.read_file(filename)
    image = tf.image.decode_png(image_string, channels=3)
    image = tf.image.convert_image_dtype(image, tf.float32)
    image = tfa.image.rotate(image, label)
    image = tf.image.central_crop(image, 0.8)
    image = tf.image.resize(image, TARGET_SHAPE)
    return (image, label)


def label_process(label):
    if(label[0] == 1.0):
        return math.radians(-12.5)
    if(label[1] == 1.0):
        return math.radians(-10.0)
    if(label[2] == 1.0):
        return math.radians(-7.5)
    if(label[3] == 1.0):
        return math.radians(-5.0)
    if(label[4] == 1.0):
        return math.radians(-2.5)
    if(label[5] == 1.0):
        return math.radians(0.0)
    if(label[6] == 1.0):
        return math.radians(2.5)
    if(label[7] == 1.0):
        return math.radians(5.0)
    if(label[8] == 1.0):
        return math.radians(7.5)
    if(label[9] == 1.0):
        return math.radians(10.0)
    return math.radians(0.0)


def preprocess_image_with_angle1(filename, label):
    # filename, label = input1
    print(filename, label)
    """
    Load the specified file as a JPEG image, preprocess it and
    resize it to the target shape.
    """

    image_string = tf.io.read_file(filename)
    image = tf.image.decode_png(image_string, channels=1)
    image = tf.image.convert_image_dtype(image, tf.float32)
    image = tfa.image.rotate(image, label_process(label))
    image = tf.image.central_crop(image, 0.8)
    image = tf.image.resize(image, TARGET_SHAPE)
    return image


def preprocess_image_with_angle2(filename, label):
    # filename, label = input1
    print(filename, label)
    """
    Load the specified file as a JPEG image, preprocess it and
    resize it to the target shape.
    """

    image_string = tf.io.read_file(filename)
    image = tf.image.decode_png(image_string, channels=1)
    image = tf.image.convert_image_dtype(image, tf.float32)
    image = tfa.image.rotate(image, label_process(label))
    image = tf.image.central_crop(image, 0.8)
    image = tf.image.resize(image, TARGET_SHAPE)
    return (label, image)


def contains_string(fullstr: str, substr: str):
    if substr in fullstr:
        return True
    else:
        return False


def preprocess_triplets(anchor, positive, negative):
    """
    Given the filenames corresponding to the three images, load and
    preprocess them.
    """

    return (
        preprocess_image(anchor),
        preprocess_image(positive),
        preprocess_image(negative),
    )


def preprocess_angles(image, label):
    """
    Given the filenames corresponding to the three images, load and
    preprocess them.
    """

    return preprocess_image_with_angle(image, label)


def preprocess_angles1(image, label):
    """
    Given the filenames corresponding to the three images, load and
    preprocess them.
    """

    return preprocess_image_with_angle1(image, label)


def preprocess_angles2(image, label):
    """
    Given the filenames corresponding to the three images, load and
    preprocess them.
    """

    return preprocess_image_with_angle2(label, image)


def get_dataset_with_transformations(data_dir):
    path = Path(Path.home() / "Documents" /
                "magistras" / "master_capsnet" / data_dir)
    images, labels = get_image_triplet_list_for_transformation(data_dir)
    print(images[0], labels[0])
    # print(images)
    image_count = len(images[0])
    images = list(map(lambda image: str(path / image), images))
    # print(images)
    images_dataset = tf.data.Dataset.from_tensor_slices((images, labels))
    labels_dataset = tf.data.Dataset.from_tensor_slices(labels)
    # images_dataset = tf.data.Dataset.zip(
    #     (images_dataset, labels_dataset))
    # labels_dataset = tf.data.Dataset.zip(
    #     (labels_dataset, images_dataset))
    # dataset = dataset.shuffle(buffer_size=5000)
    images_dataset = images_dataset.map(preprocess_angles1)
    return images_dataset, labels_dataset, image_count


def get_dataset(data_dir):
    path = Path(Path.home() / "Documents" /
                "magistras" / "master_capsnet" / data_dir)
    anchor_images, positive_images, negative_images = get_image_triplet_list_2(
        data_dir)
    image_count = len(anchor_images)
    anchor_images_sorted = sorted(
        [str(path / f) for f in anchor_images]
    )
    positive_images_sorted = sorted(
        [str(path / f) for f in positive_images]
    )
    negative_images_sorted = sorted(
        [str(path / f) for f in negative_images]
    )
    anchor_dataset = tf.data.Dataset.from_tensor_slices(anchor_images_sorted)
    positive_dataset = tf.data.Dataset.from_tensor_slices(
        positive_images_sorted)
    negative_dataset = tf.data.Dataset.from_tensor_slices(
        negative_images_sorted)
    dataset = tf.data.Dataset.zip(
        (anchor_dataset, positive_dataset, negative_dataset))
    dataset = dataset.shuffle(buffer_size=1024)
    dataset = dataset.map(preprocess_triplets)
    return dataset, image_count


def get_train_valid_datasets(x_ds, y_ds, image_count, batch_size):
    # dataset, image_count = get_dataset(data_dir)
    # Let's now split dataset in train and validation.
    x_train = x_ds.take(round(image_count * 0.8))
    y_train = y_ds.take(round(image_count * 0.8))
    x_valid = x_ds.skip(round(image_count * 0.8))
    y_valid = y_ds.skip(round(image_count * 0.8))
    x_train = x_train.batch(batch_size, drop_remainder=False)
    x_train = x_train.prefetch(tf.data.AUTOTUNE)
    y_train = y_train.batch(batch_size, drop_remainder=False)
    y_train = y_train.prefetch(tf.data.AUTOTUNE)
    x_valid = x_valid.batch(batch_size, drop_remainder=False)
    x_valid = x_valid.prefetch(tf.data.AUTOTUNE)
    y_valid = y_valid.batch(batch_size, drop_remainder=False)
    y_valid = y_valid.prefetch(tf.data.AUTOTUNE)
    return x_train, y_train, x_valid, y_valid


def get_test_dataset(data_dir):
    dataset, image_count = get_dataset(data_dir)
    test_dataset = dataset.batch(32, drop_remainder=False)
    test_dataset = dataset.prefetch(tf.data.AUTOTUNE)
    return test_dataset


def load_data(data_dir, valid_dir, batch_size):
    print("load_data: " + data_dir)
    triples_train = get_image_triplet_list(data_dir)
    triples_test = get_image_triplet_list(valid_dir)
    num_steps_train = len(triples_train) // batch_size
    num_steps_val = len(triples_test) // batch_size
    train_gen = generate_image_triples_batch(
        data_dir, triples_train, batch_size, shuffle=True)
    val_gen = generate_image_triples_batch(
        data_dir, triples_test, batch_size, shuffle=False)
    return train_gen, val_gen, num_steps_train, num_steps_val


def load_data_siamese(data_dir):
    return get_dataset(data_dir)


def visualize_triplet(anchor, positive, negative):
    """Visualize a few triplets from the supplied batches."""

    def show(ax, image):
        ax.imshow(image)
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)

    fig = plt.figure(figsize=(9, 9))

    axs = fig.subplots(3, 1)
    for i in range(1):
        show(axs[i, 0], anchor[i])
        show(axs[i, 1], positive[i])
        show(axs[i, 2], negative[i])
    plt.show()


def visualize_image(image, label):
    """Visualize a image from the supplied batches."""
    print(type(image), image.shape)
    image2 = np.squeeze(image, axis=0)
    print(image2)
    plt.imshow(image2)
    plt.show()


def show_images(images, cols=1):
    """Display a list of images in a single figure with matplotlib.

    Parameters
    ---------
    images: List of np.arrays compatible with plt.imshow.

    cols (Default = 1): Number of columns in figure (number of rows is 
                        set to np.ceil(n_images/float(cols))).

    titles: List of titles corresponding to each image. Must have
            the same length as titles.
    """
    n_images = len(images)
    # if titles is None:
    #     titles = ['Image (%d)' % i for i in range(1, n_images + 1)]
    fig = plt.figure()
    for n, (image) in enumerate(images):
        print(image)
        # print(n, type(image[0][0]), image[0][0].shape)
        # print('label', image[0][1][0])
        a = fig.add_subplot(cols, np.ceil(n_images/float(cols)), n + 1)
        # image2 = np.squeeze(image, axis=0)
        # print(type(image2), image2.shape)
        if image.ndim == 2:
            plt.gray()
        plt.imshow(image)
        a.set_title('a')
    fig.set_size_inches(np.array(fig.get_size_inches()))
    plt.show()


def visualize_images(images):
    show_images(images)
