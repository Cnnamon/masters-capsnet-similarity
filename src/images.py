import random
from PIL import Image
import tensorflow as tf
import tensorflow_addons as tfa
import math
import numpy as np
from utils.utils import combine_images, plot_log
import os
from pathlib import Path
import matplotlib.pyplot as plt

data_dir_train = './data/train'
data_dir_valid = './data/valid'
data_dir_test = './data/test'
n_angles = 10
BATCH_SIZE = 100

RESIZE_WIDTH = 32  # 28  # 128  # 224
RESIZE_HEIGHT = 32  # 28  # 128  # 224
TARGET_SHAPE = (RESIZE_WIDTH, RESIZE_HEIGHT)

all_files = {}


def get_images_and_labels(data_dir):
    path = Path(Path.home() / "Documents" /
                "magistras" / "master_capsnet" / data_dir)
    images = np.array([])
    for image_dir in os.listdir(path):
        images = np.append(images, [image_dir], axis=0)
    # n_angles = 10
    # n_class = len(images)
    # labels = tf.keras.utils.to_categorical(
    #     np.arange(n_class), num_classes=n_class)
    # resize adds using the same pattern
    # labels = np.resize(labels, (len(images) * n_angles, n_class))
    # repeat repeats each item by times
    # images = np.repeat(images, n_angles)
    images = np.array(list(map(lambda image: str(path / image), images)))
    return images


images_train = get_images_and_labels(data_dir_train)
images_valid = get_images_and_labels(data_dir_valid)
images_test = get_images_and_labels(data_dir_test)
# len(images_train), images_train.shape


def angle_process(index, n_angles):
    angle_0 = n_angles / 2
    angle_multi = index % angle_0
    if((index % n_angles) < angle_0):
        return math.radians(-2.5 * angle_multi)
    if((index % n_angles) > angle_0):
        return math.radians(2.5 * angle_multi)
    return math.radians(0)


def preprocess_image_with_angle(filename, index, n_angles, channels=3):
    # print(filename, label)
    """
    Load the specified file as a JPEG image, preprocess it and
    resize it to the target shape.
    """

    def get_image():
        if filename in all_files:
            return all_files[filename]
        else:
            image_string = tf.io.read_file(filename)
            image = tf.image.decode_png(image_string, channels=channels)
            image = tf.image.convert_image_dtype(image, tf.float32)
            image = tf.image.resize(image, (64, 64))
            x_im = np.asarray(image)
            means = x_im.mean(axis=(0, 1), dtype='float64')
            std = x_im.std(axis=(0, 1), dtype='float64')
            x_im = (x_im - means) / std
            x_im = (x_im - x_im.min()) / (x_im.max() - x_im.min())
            all_files[filename] = x_im
            return x_im
    image = get_image()
    image = tfa.image.rotate(image, angle_process(index, n_angles))
    image = tf.image.central_crop(image, 0.8)
    image = tf.image.resize(image, TARGET_SHAPE)
    return image


def get_images_eval(channels=3):
    images_eval = []
    for i in range(10):
        for angle in range(n_angles):
            image = preprocess_image_with_angle(
                images_train[i], angle, n_angles, channels)
            images_eval.append(image)
    images_eval = np.array(images_eval)
    return images_eval


def predict_and_print(save_dir, eval_model, x_test, file_name):
    x_recon = eval_model.predict(x_test, batch_size=100)
    print(x_recon.shape)
    print('-' * 30 + 'Begin: test' + '-' * 30)
    img = combine_images(np.concatenate([x_test[:50], x_recon[:50]]))
    image = img * 255
    Image.fromarray(image.astype(np.uint8)).save(
        save_dir + "/" + file_name + ".png")
    print('Reconstructed images are saved to %s/%s.png' %
          (save_dir, file_name))
    print('-' * 30 + 'End: test' + '-' * 30)
    plt.imshow(plt.imread(save_dir + "/" + file_name + ".png"))
    plt.show()


def create_batch(images, shape, channels=3):  # *images_eval.shape[1:]
    x_a = np.zeros((BATCH_SIZE, *shape))  # * - expands tuple
    x_p = np.zeros((BATCH_SIZE, *shape))  # * - expands tuple
    x_n = np.zeros((BATCH_SIZE, *shape))  # * - expands tuple
    # print(x_anchors.shape)

    for i in range(0, 10):
        for angle in range(0, n_angles):
            triplet_i = random.randint(0, (len(images) - 3) / 3)
            image_a = preprocess_image_with_angle(
                images[triplet_i*3], angle, n_angles, channels)  # anchor angle is transformed
            image_p = preprocess_image_with_angle(
                images[triplet_i*3+1], 0, n_angles, channels)  # positive angle is 0
            image_n = preprocess_image_with_angle(
                images[triplet_i*3+2], 0, n_angles, channels)  # negative angle is 0
            x_a[i*10 + angle] = image_a
            x_p[i*10 + angle] = image_p
            x_n[i*10 + angle] = image_n
    return np.array([x_a, x_p, x_n])


def combine_images3(generated_images, height=None, width=None):
    num = generated_images.shape[0]
    if width is None and height is None:
        width = int(math.sqrt(num))
        height = int(math.ceil(float(num)/width))
    elif width is not None and height is None:  # height not given
        height = int(math.ceil(float(num)/width))
    elif height is not None and width is None:  # width not given
        width = int(math.ceil(float(num)/height))

    shape = generated_images.shape[1:3]
    image = np.zeros((height*shape[0], width*shape[1], 3),
                     dtype=generated_images.dtype)
    for index, img in enumerate(generated_images):
        i = int(index/width)
        j = index % width
        image[i*shape[0]:(i+1)*shape[0], j*shape[1]:(j+1)*shape[1], 0] = \
            img[:, :, 0]
        image[i*shape[0]:(i+1)*shape[0], j*shape[1]:(j+1)*shape[1], 1] = \
            img[:, :, 1]
        image[i*shape[0]:(i+1)*shape[0], j*shape[1]:(j+1)*shape[1], 2] = \
            img[:, :, 2]
    return image
