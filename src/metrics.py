from tensorflow.keras import callbacks
import tensorflow as tf

emb_size = 32
alpha = 0.2


def triplet_loss(y_true, y_pred):
    anchor, positive, negative = y_pred[:, :emb_size], y_pred[:,
                                                              emb_size:2*emb_size], y_pred[:, 2*emb_size:]
    positive_dist = tf.reduce_mean(tf.square(anchor - positive), axis=1)
    negative_dist = tf.reduce_mean(tf.square(anchor - negative), axis=1)
    L = tf.maximum(positive_dist - negative_dist + alpha, 0.)
    return L


def accuracy(y_true, y_pred):
    anchor, positive, negative = y_pred[:, :emb_size], y_pred[:,
                                                              emb_size:2*emb_size], y_pred[:, 2*emb_size:]
    positive_dist = tf.reduce_mean(tf.square(anchor - positive), axis=1)
    negative_dist = tf.reduce_mean(tf.square(anchor - negative), axis=1)
    return positive_dist + alpha < negative_dist


def get_callbacks(save_dir='./result/capsnet_cifar', lr=0.001, lr_decay=0.9, monitor='val_capsnet_accuracy'):
    log_dir = save_dir + '/logs'
    # callbacks
    log_cb = callbacks.CSVLogger(save_dir + '/log.csv')
    checkpoint_cb = callbacks.ModelCheckpoint(save_dir + '/weights-{epoch:02d}.h5', monitor=monitor,
                                              save_best_only=True, save_weights_only=True, verbose=1)
    lr_decay_cb = callbacks.LearningRateScheduler(
        schedule=lambda epoch: lr * (lr_decay ** epoch))
    tensorboard_cb = callbacks.TensorBoard(
        log_dir=log_dir, histogram_freq=1)
    return [log_cb, checkpoint_cb, lr_decay_cb, tensorboard_cb]


def margin_loss(y_true, y_pred):
    """
    Margin loss for Eq.(4). When y_true[i, :] contains not just one `1`, this loss should work too. Not test it.
    :param y_true: [None, n_classes]
    :param y_pred: [None, num_capsule]
    :return: a scalar loss value.
    """
    L = y_true * tf.square(tf.maximum(0., 0.9 - y_pred)) + \
        0.5 * (1 - y_true) * tf.square(tf.maximum(0., y_pred - 0.1))
    mean = tf.reduce_mean(tf.reduce_sum(L, 1))
    return mean
